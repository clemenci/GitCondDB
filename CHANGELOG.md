# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased][]

## [0.2.2][] - 2023-05-26
Patch release to pick up bug fixes.

### Fixed
- Fix compilation with gcc 13 and C++20 (lhcb/GitCondDB#20)

## [0.2.1][] - 2022-10-25
This patch release features updates of the verions of tools used internally
(test, formatting etc.) plus the fix of a segfault on destruction.

### Changed
- Update Catch2 to v3.0.1 (lhcb/GitCondDB!41)
- Update of tools and deps (lhcb/GitCondDB!36) 

### Added
- Add optional support for old-style LHCb install layout (lhcb/GitCondDB!39)

### Fixed
- Segmentation fault at distruction (lhcb/GitCondDB#19)

## [0.2.0][] - 2020-06-23
Apart for a couple of minor updates (lhcb/GitCondDB!31, lhcb/GitCondDB!32),
this release introduces the possibility of using filenames, instead of the
cryptic `IOVs` file, to map payloads to IOVs in a condition directory.

### Added
- Allow use of filenames instead of the special IOVs file (lhcb/GitCondDB#10)

## [0.1.3][] - 2020-04-21

### Fixed
- Hide warnings from Catch2 (lhcb/GitCondDB!28)
- Fix version number in `CMakeLists.txt` (lhcb/GitCondDB#14)
- Look for `fmt` in  `GitCondDBConfig.cmake` (lhcb/GitCondDB#15)

## [0.1.2][] - 2020-04-14

### Changed
- Use [Catch2](https://github.com/catchorg/Catch2) for tests (lhcb/GitCondDB#8)

### Added
- Add more platforms to CI builds (lhcb/GitCondDB#11)
- Add support for MacOS and Clang (lhcb/GitCondDB!19, lhcb/GitCondDB!20)

### Fixed
- Add integration tests to make sure the generated CMake files work (lhcb/GitCondDB#9)

## [0.1.1][] - 2019-04-11

### Fixed
- Make `CondDB` moveable and add test that checks it (lhcb/GitCondDB!17)

## 0.1.0 - 2019-03-12
First official version.

### Changed
- Require fmtlib version to 5.2(.1) (lhcb/GitCondDB!16)

### Fixed
- Improvements to Gitlab-CI (lhcb/GitCondDB!15)
- Improve exported GitCondDB target interface (lhcb/GitCondDB!13)
- Use imported Target for libgit2 (lhcb/GitCondDB!12)
- Enable download of GoogleTest (lhcb/GitCondDB!11)

[Unreleased]: https://gitlab.cern.ch/lhcb/GitCondDB/compare/0.2.1...master
[0.2.1]: https://gitlab.cern.ch/lhcb/GitCondDB/compare/0.2.0...0.2.1
[0.2.0]: https://gitlab.cern.ch/lhcb/GitCondDB/compare/0.1.3...0.2.0
[0.1.3]: https://gitlab.cern.ch/lhcb/GitCondDB/compare/0.1.2...0.1.3
[0.1.2]: https://gitlab.cern.ch/lhcb/GitCondDB/compare/0.1.1...0.1.2
[0.1.1]: https://gitlab.cern.ch/lhcb/GitCondDB/compare/0.1.0...0.1.1
