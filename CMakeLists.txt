###############################################################################
# (c) Copyright 2018-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the Apache version 2        #
# licence, copied verbatim in the file "COPYING".                             #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Project setup
cmake_minimum_required(VERSION 3.14)
project(GitCondDB VERSION 0.2.2)

if(NOT DEFINED CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 17)
endif()

# Find/use git if available
find_package(Git QUIET)
if(Git_FOUND)
  if(GIT_VERSION_STRING VERSION_GREATER_EQUAL 1.8.4)
    set(GitCondDB_GIT_DESCRIBE_FPARG "--first-parent")
  endif()

  execute_process(COMMAND ${GIT_EXECUTABLE} describe --tags --always ${GitCondDB_GIT_DESCRIBE_FPARG} HEAD
    WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}"
    OUTPUT_VARIABLE GitCondDB_GIT_VERSION OUTPUT_STRIP_TRAILING_WHITESPACE)
endif()

# Project modules
include(GenerateExportHeader)
include(GNUInstallDirs)

# Project optional/fixed settings
option(BUILD_SHARED_LIBS "Build shared library" ON)
option(CMAKE_EXPORT_COMPILE_COMMANDS "" ON)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wpedantic")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Og")
set(CMAKE_CXX_FLAGS_COVERAGE "${CMAKE_CXX_FLAGS_COVERAGE} -g -O0 --coverage")

set(CMAKE_CXX_VISIBILITY_PRESET hidden)
set(CMAKE_VISIBILITY_INLINES_HIDDEN 1)

set(CMAKE_CXX_EXTENSIONS OFF)

# external dependencies

find_package(PkgConfig)
pkg_check_modules(git2 libgit2 REQUIRED IMPORTED_TARGET)

# Check for the C++ filesystem implementation
# 1. Full Native
try_compile(CPPFS_IS_NATIVE
  ${CMAKE_CURRENT_BINARY_DIR}/_cppfs_check/native
  ${PROJECT_SOURCE_DIR}/cmake/cppfs-check.cpp
  CMAKE_FLAGS -DCMAKE_CXX_STANDARD=${CMAKE_CXX_STANDARD} -DCMAKE_CXX_STANDARD_REQUIRED=ON)

# 2. Native plus support library (GNU/Clang)
if(NOT CPPFS_IS_NATIVE)
  # GNU libstdc++fs
  try_compile(CPPFS_IS_STDCPPFS
    ${CMAKE_CURRENT_BINARY_DIR}/_cppfs_check/stdc++fs
    ${PROJECT_SOURCE_DIR}/cmake/cppfs-check.cpp
    CMAKE_FLAGS -DCMAKE_CXX_STANDARD=${CMAKE_CXX_STANDARD} -DCMAKE_CXX_STANDARD_REQUIRED=ON
    LINK_LIBRARIES stdc++fs
    )
  # LLVM libc++fs
  try_compile(CPPFS_IS_CPPFS
    ${CMAKE_CURRENT_BINARY_DIR}/_cppfs_check/c++fs
    ${PROJECT_SOURCE_DIR}/cmake/cppfs-check.cpp
    CMAKE_FLAGS -DCMAKE_CXX_STANDARD=${CMAKE_CXX_STANDARD} -DCMAKE_CXX_STANDARD_REQUIRED=ON
    LINK_LIBRARIES c++fs
    )
  # Fallback to Boost otherwsie
  if(NOT CPPFS_IS_STDCPPFS AND NOT CPPFS_IS_CPPFS)
    find_package(Boost 1.63 REQUIRED filesystem)
    set(CPPFS_IS_BOOSTFS TRUE)
  endif()

  # Set up needed linking and support
  if(CPPFS_IS_STDCPPFS)
    set(GitCondDB_FSImpl stdc++fs)
  elseif(CPPFS_IS_CPPFS)
    set(GitCondDB_FSImpl c++fs)
  elseif(CPPFS_IS_BOOSTFS)
    set(GitCondDB_FSImpl Boost::filesystem)
    configure_file(cmake/GitCondDBFSShim.cmake.in
      "${CMAKE_CURRENT_BINARY_DIR}/GitCondDB/GitCondDBFSShim.cmake" @ONLY)
  else()
    message(FATAL_ERROR "No compiler or Boost support for C++ filesystem")
  endif()
endif()

find_path(JSON_INCLUDE_DIR NAMES nlohmann/json.hpp)
if(NOT JSON_INCLUDE_DIR)
  set(NLOHMANN_JSON_URL https://github.com/nlohmann/json/releases/download/v3.11.2/json.hpp
      CACHE STRING "URL used to download nlohmann/json.hpp")
  file(DOWNLOAD ${NLOHMANN_JSON_URL} "${PROJECT_BINARY_DIR}/nlohmann/json.hpp"
    SHOW_PROGRESS STATUS JSON_HPP_DOWNLOAD_STATUS
    EXPECTED_HASH SHA256=665fa14b8af3837966949e8eb0052d583e2ac105d3438baba9951785512cf921)
  list(GET JSON_HPP_DOWNLOAD_STATUS 0 JSON_HPP_DOWNLOAD_STATUS)
  if(NOT JSON_HPP_DOWNLOAD_STATUS EQUAL 0)
    message(FATAL_ERROR "Could not download json.hpp")
  endif()
  set(JSON_INCLUDE_DIR "${PROJECT_BINARY_DIR}" CACHE PATH "Path to nlohmann/json.hpp" FORCE)
endif()
include_directories("${JSON_INCLUDE_DIR}")

find_package(fmt 5.2 REQUIRED)


# Build instructions
# Generate a version header
configure_file(cmake/GitCondDBVersion.h.in GitCondDBVersion.h)

set(HEADERS include/GitCondDB.h "${CMAKE_CURRENT_BINARY_DIR}/GitCondDBVersion.h")
set(SOURCES src/common.h src/git_helpers.h src/iov_helpers.h src/path_helpers.h src/DBImpl.h src/BasicLogger.h src/GitCondDB.cpp)

# Target, export header, and alias for importing as subproject
add_library(GitCondDB ${HEADERS} ${SOURCES})
generate_export_header(GitCondDB)
add_library(GitCondDB::GitCondDB ALIAS GitCondDB)

# Usage requirements, properties
target_compile_definitions(GitCondDB PRIVATE $<$<BOOL:${CPPFS_IS_BOOSTFS}>:CPPFS_IS_BOOSTFS>)
target_compile_features(GitCondDB PUBLIC cxx_std_${CMAKE_CXX_STANDARD})
target_include_directories(GitCondDB PUBLIC
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include>
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
  $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>)
target_link_libraries(GitCondDB PRIVATE PkgConfig::git2 fmt::fmt)
target_link_libraries(GitCondDB PUBLIC ${GitCondDB_FSImpl})

set_property(TARGET GitCondDB PROPERTY VERSION ${GitCondDB_VERSION})
set_property(TARGET GitCondDB PROPERTY SOVERSION 1)
set_property(TARGET GitCondDB PROPERTY INTERFACE_GitCondDB_MAJOR_VERSION 1)
set_property(TARGET GitCondDB APPEND PROPERTY COMPATIBLE_INTERFACE_STRING GitCondDB_MAJOR_VERSION)
set_property(TARGET GitCondDB PROPERTY PUBLIC_HEADER ${HEADERS} "${CMAKE_CURRENT_BINARY_DIR}/gitconddb_export.h")


# installation

install(TARGETS GitCondDB EXPORT GitCondDBTargets
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    COMPONENT Runtime
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
    COMPONENT Devel
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
    COMPONENT Runtime
  PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
    COMPONENT Devel
  INCLUDES DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
)


# package exports

include(CMakePackageConfigHelpers)

set(ConfigPackageLocation ${CMAKE_INSTALL_LIBDIR}/cmake/GitCondDB)

write_basic_package_version_file(
  "${CMAKE_CURRENT_BINARY_DIR}/GitCondDB/GitCondDBConfigVersion.cmake"
  VERSION ${GitCondDB_VERSION}
  COMPATIBILITY AnyNewerVersion
)
configure_package_config_file(cmake/GitCondDBConfig.cmake.in
  "${CMAKE_CURRENT_BINARY_DIR}/GitCondDB/GitCondDBConfig.cmake"
  INSTALL_DESTINATION ${ConfigPackageLocation}
)

export(EXPORT GitCondDBTargets
    NAMESPACE GitCondDB::
    FILE "${CMAKE_CURRENT_BINARY_DIR}/GitCondDB/GitCondDBTargets.cmake")

install(EXPORT GitCondDBTargets
  NAMESPACE GitCondDB::
    DESTINATION ${ConfigPackageLocation})

install(DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/GitCondDB/"
  DESTINATION "${ConfigPackageLocation}"
  COMPONENT Devel
  PATTERN "GitCondDBTargets*" EXCLUDE
)

# testing
include(CTest)

# disable tests when built as a subdirectory, unless we are
# asked to trust BUILD_TESTINGS
if(NOT CMAKE_PROJECT_NAME STREQUAL "GitCondDB"
      AND NOT GITCONDDB_FOLLOW_BUILD_TESTING)
  set(BUILD_TESTING NO)
endif()

if(BUILD_TESTING)
  message(STATUS "Tests are enabled")

  # - prepare test repositories
  if(NOT CMAKE_VERSION VERSION_LESS 3.12)
    find_package(Python REQUIRED COMPONENTS Interpreter)
  else()
    find_package(PythonInterp REQUIRED)
    add_executable(Python::Interpreter IMPORTED)
    set_property(TARGET Python::Interpreter PROPERTY IMPORTED_LOCATION "${PYTHON_EXECUTABLE}")
  endif()

  add_custom_command(
    COMMENT "Generating test data"
    OUTPUT ${PROJECT_BINARY_DIR}/test_data/.stamp
    COMMAND Python::Interpreter ${PROJECT_SOURCE_DIR}/tests/prepare_test_data.py
    COMMAND ${CMAKE_COMMAND} -E touch test_data/.stamp
    DEPENDS tests/prepare_test_data.py)

  add_custom_target(TestData DEPENDS ${PROJECT_BINARY_DIR}/test_data/.stamp)

  include(FetchContent)

  FetchContent_Declare(
    Catch2
    # GIT_REPOSITORY https://github.com/catchorg/Catch2.git
    GIT_REPOSITORY https://gitlab.cern.ch/lhcb-core/mirrors/Catch2.git
    GIT_TAG        v3.3.2 # or a later release
  )

  FetchContent_MakeAvailable(Catch2)
  # Make sure Catch2 is built with the right C++ standard
  target_compile_features(Catch2 PUBLIC cxx_std_${CMAKE_CXX_STANDARD})

  include(Catch)

  foreach(subsystem IN ITEMS CondDB CondDBMove FS Git Helpers JSON DirectIOVs PathHelpers)
    add_executable(test_${subsystem}
        src/tests/test_common.h src/tests/${subsystem}_UnitTests.cpp)
    target_compile_definitions(test_${subsystem} PRIVATE $<$<BOOL:${CPPFS_IS_BOOSTFS}>:CPPFS_IS_BOOSTFS>)
    target_include_directories(test_${subsystem} PRIVATE include src)
    target_link_libraries(test_${subsystem} GitCondDB PkgConfig::git2 fmt::fmt Catch2::Catch2WithMain)
    add_dependencies(test_${subsystem} TestData)
    catch_discover_tests(
      test_${subsystem}
      TEST_PREFIX ${PROJECT_NAME}.
      PROPERTIES LABELS ${PROJECT_NAME}
    )
  endforeach()

  # - coverage reports
  if(CMAKE_BUILD_TYPE STREQUAL "Coverage")
    find_program(lcov_COMMAND NAMES lcov)
    find_program(genhtml_COMMAND NAMES genhtml)
    if(lcov_COMMAND AND genhtml_COMMAND)
      add_custom_target(clean-coverage-stats
        COMMAND ${lcov_COMMAND} --zerocounters --directory .
        COMMENT "Cleaning coverage statistics")
      add_custom_target(generate-coverage-report
        COMMAND ${lcov_COMMAND} --quiet --capture --directory . --output-file coverage.info
        COMMAND ${lcov_COMMAND} --quiet --output-file coverage.info --remove coverage.info '*/catch2/*' '/usr/*' '*/json.hpp' '*/BasicLogger.h' '${PROJECT_SOURCE_DIR}/src/tests/*'
        COMMAND ${CMAKE_COMMAND} -E remove_directory coverage_report
        COMMAND ${genhtml_COMMAND} coverage.info --output-directory coverage_report
        COMMENT "Generate coverage report")
      add_custom_target(coverage-report
        COMMAND ${CMAKE_COMMAND} --build . --target clean-coverage-stats
        COMMAND ${CMAKE_COMMAND} --build . --target test || true
        COMMAND ${CMAKE_COMMAND} --build . --target generate-coverage-report)
    endif()
  endif()

endif(BUILD_TESTING)

# Optionally enable compatibility with old-style LHCb install layout, via helper module
option(GAUDI_LEGACY_CMAKE_SUPPORT "Enable compatibility with old-style CMake builds" "$ENV{GAUDI_LEGACY_CMAKE_SUPPORT}")
if(GAUDI_LEGACY_CMAKE_SUPPORT)
  find_file(legacy_cmake_config_support NAMES LegacyGaudiCMakeSupport.cmake)
  if(legacy_cmake_config_support)
    include(${legacy_cmake_config_support})
  else()
    message(FATAL_ERROR "GAUDI_LEGACY_CMAKE_SUPPORT set to TRUE, but cannot find LegacyGaudiCMakeSupport.cmake")
  endif()
endif()
