#ifndef PATH_HELPERS_H
#define PATH_HELPERS_H
/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the Apache version 2        *
* licence, copied verbatim in the file "COPYING".                             *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <regex>
#include <string_view>
#include <tuple>

namespace GitCondDB {
  namespace Helpers {
    /// Remove the optional "<tag>:" prefix from an object_id.
    inline std::string_view strip_tag( std::string_view object_id ) {
      if ( const auto pos = object_id.find_first_of( ':' ); pos != object_id.npos ) {
        object_id.remove_prefix( pos + 1 );
      }
      return object_id;
    }

    /// Split an object_id in parent id and basename.
    /// - from "<tag>:<dir>[/<dir>]/<file>" to ("<tag>:<dir>[/<dir>]", "<file>")
    /// - from "<tag>:<file>" to ("<tag>:", "<file>")
    /// - from "<tag>:" to ("<tag>:", "")
    inline std::tuple<std::string_view, std::string_view> split_id( std::string_view object_id ) {
      if ( const auto pos = object_id.find_last_of( "/:" ); pos != object_id.npos ) {
        const auto tail_start = pos + 1;
        const auto head_end   = pos + ( object_id[pos] == ':' ? 1 : 0 ); // ':' is special, we have to keep it
        return { object_id.substr( 0, head_end ), object_id.substr( tail_start ) };
      }
      return { object_id, {} };
    }

    /// helper to normalize relative paths
    std::string normalize( std::string path ) {
      // regex for entries to be removed, i.e. "/parent/../" and "/./"
      // (with special handling for things like "<tag>:parent/../" and "paremt/../")
      static const std::regex ignored_re{ "([/:]|^)(?:[^/:]+/\\.\\./|\\./)" };
      std::string             old_path;
      while ( old_path.length() != path.length() ) {
        old_path.swap( path );
        path = std::regex_replace( old_path, ignored_re, "$1" );
      }
      return path;
    }
  } // namespace Helpers
} // namespace GitCondDB

#endif
