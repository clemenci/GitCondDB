/*****************************************************************************\
* (c) Copyright 2018 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the Apache version 2        *
* licence, copied verbatim in the file "COPYING".                             *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <optional>

#include "GitCondDB.h"

#include <catch2/catch_test_macros.hpp>

using namespace GitCondDB::v1;

TEST_CASE( "Move" ) {
  // Making sure the move constructor for the CondDB is functional
  auto temporary_db = connect( "test_data/repo.git" );
  auto db           = std::move( temporary_db );

  CHECK( db.connected() );
  CHECK( std::get<0>( db.get( { "HEAD", "TheDir/TheFile.txt", 0 } ) ) == "some data\n" );
}

TEST_CASE( "Optional" ) {
  // Check that we can use the db in a std::optional, this requires it can be moved.
  std::optional<CondDB> db = std::nullopt;
  db.emplace( connect( "test_data/repo.git" ) );

  CHECK( db->connected() );
  CHECK( std::get<0>( db->get( { "HEAD", "TheDir/TheFile.txt", 0 } ) ) == "some data\n" );
}
