/*****************************************************************************\
* (c) Copyright 2018 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the Apache version 2        *
* licence, copied verbatim in the file "COPYING".                             *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "GitCondDB.h"

#include "DBImpl.h"
#include "iov_helpers.h"

#include "test_common.h"

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>

using namespace GitCondDB::v1;

TEST_CASE( "Connection" ) {
  SECTION( "data from file" ) {
    auto logger = std::make_shared<CapturingLogger>();

    details::JSONImpl db{ "test_data/json/minimal.json", logger };
    REQUIRE( logger->size() == 1 );
    REQUIRE( logger->contains( "loading JSON data from " ) );
  }

  SECTION( "data from memory" ) {
    auto logger = std::make_shared<CapturingLogger>();

    details::JSONImpl db{ "{}", logger };
    SECTION( "logging correct" ) {
      REQUIRE( logger->size() == 1 );
      REQUIRE( logger->contains( "using JSON data from memory" ) );
    }
    SECTION( "is connected" ) { REQUIRE( db.connected() ); }
    SECTION( "no-op disconnect" ) {
      db.disconnect();
      REQUIRE( db.connected() );
    }
    SECTION( "has fake HEAD version" ) {
      REQUIRE( db.exists( "HEAD" ) );
      REQUIRE( db.connected() );
    }
  }
}

TEST_CASE( "access failure" ) { //
  CHECK_THROWS_WITH( details::JSONImpl{ "test_data/json/no-file" }, "invalid JSON" );
}

TEST_CASE( "access memory" ) {
  auto logger = std::make_shared<CapturingLogger>();

  details::JSONImpl db{
      R"({
      "TheDir": {
        "TheFile.txt": "JSON data\n"
      },
      "Cond": {
        "IOVs": "0 a\n100 b\n",
        "a": "data a",
        "b": "data b"
      },
      "BadType": 123
    })",
      logger };

  SECTION( "connection log" ) {
    CHECK( logger->size() == 1 );
    CHECK( logger->contains( "using JSON data from memory" ) );
  }

  logger->logged_messages.clear();

  SECTION( "get file from HEAD" ) {
    CHECK( std::get<0>( db.get( "HEAD:TheDir/TheFile.txt" ) ) == "JSON data\n" );
    CHECK( logger->size() == 2 );
    CHECK( logger->contains( 0, "accessing entry '/TheDir/TheFile.txt'" ) );
    CHECK( logger->contains( 1, "found string" ) );
  }

  SECTION( "get file from fake tag" ) {
    CHECK( std::get<0>( db.get( "foobar:TheDir/TheFile.txt" ) ) == "JSON data\n" );
    CHECK( logger->size() == 2 );
    CHECK( logger->contains( 0, "accessing entry '/TheDir/TheFile.txt'" ) );
    CHECK( logger->contains( 1, "found string" ) );
  }

  SECTION( "get directory" ) {
    auto cont = std::get<1>( db.get( "HEAD:TheDir" ) );
    CHECK( logger->size() == 2 );
    CHECK( logger->contains( 0, "accessing entry '/TheDir'" ) );
    CHECK( logger->contains( 1, "found object" ) );

    CHECK( cont.dirs.empty() );
    CHECK( cont.files == std::vector<std::string>{ { "TheFile.txt" } } );
    CHECK( cont.root == "TheDir" );
  }

  SECTION( "get root directory" ) {
    auto cont = std::get<1>( db.get( "HEAD:" ) );
    CHECK( logger->size() == 2 );
    CHECK( logger->contains( 0, "accessing entry ''" ) );
    CHECK( logger->contains( 1, "found object" ) );

    std::vector<std::string> expected{ "Cond", "TheDir" };
    sort( begin( cont.dirs ), end( cont.dirs ) );
    CHECK( cont.dirs == expected );
    CHECK( cont.files == std::vector<std::string>{ { "BadType" } } );
    CHECK( cont.root == "" );
  }

  SECTION( "test existence" ) {
    CHECK( db.exists( "HEAD:TheDir" ) );
    CHECK( db.exists( "HEAD:TheDir/TheFile.txt" ) );
    CHECK_FALSE( db.exists( "HEAD:NoFile" ) );
  }

  SECTION( "access no file" ) {
    CHECK_THROWS_WITH( db.get( "HEAD:Nothing" ), "cannot resolve object HEAD:Nothing" );
    CHECK( logger->size() == 1 );
    CHECK( logger->contains( "accessing entry '/Nothing'" ) );
  }

  SECTION( "access bad type" ) {
    CHECK_THROWS_WITH( db.get( "HEAD:BadType" ), "invalid type at HEAD:BadType" );
    CHECK( logger->size() == 1 );
    CHECK( logger->contains( "accessing entry '/BadType'" ) );
  }

  SECTION( "last update" ) {
    CHECK( db.commit_time( "HEAD" ) == std::chrono::time_point<std::chrono::system_clock>::max() );
  }
}
