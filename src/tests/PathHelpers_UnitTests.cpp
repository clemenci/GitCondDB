/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the Apache version 2        *
* licence, copied verbatim in the file "COPYING".                             *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "GitCondDB.h"

#include "path_helpers.h"

#include <catch2/catch_test_macros.hpp>

using namespace GitCondDB::Helpers;

TEST_CASE( "strip_tag" ) {
  CHECK( strip_tag( "tag:some/path" ) == "some/path" );
  CHECK( strip_tag( "tag:" ) == "" );
  CHECK( strip_tag( "" ) == "" );
}

TEST_CASE( "split_id" ) {
  using ret_t = decltype( split_id( {} ) );
  CHECK( split_id( "tag:some/path" ) == ret_t{ "tag:some", "path" } );
  CHECK( split_id( "tag:some" ) == ret_t{ "tag:", "some" } );
  CHECK( split_id( "tag:" ) == ret_t{ "tag:", "" } );
  CHECK( split_id( "" ) == ret_t{ "", "" } );
  CHECK( split_id( "some/path" ) == ret_t{ "some", "path" } );
  CHECK( split_id( "some" ) == ret_t{ "some", "" } );
}

TEST_CASE( "normalize" ) {
  CHECK( normalize( "some/path" ) == "some/path" );
  CHECK( normalize( "some/other/../path" ) == "some/path" );
  CHECK( normalize( "some/./path" ) == "some/path" );
  CHECK( normalize( "/./some/path" ) == "/some/path" );
  CHECK( normalize( "/some/../other/../path" ) == "/path" );
  CHECK( normalize( "./some/path" ) == "some/path" );
  CHECK( normalize( "some/../other/../path" ) == "path" );
  CHECK( normalize( "HEAD:./some/path" ) == "HEAD:some/path" );
  CHECK( normalize( "HEAD:some/../other/../path" ) == "HEAD:path" );
}
